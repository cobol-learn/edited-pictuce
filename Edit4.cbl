       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Edit1.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  STARS PIC   *****.
       01  NUM-OF-STAR PIC   9.


       PROCEDURE DIVISION .
       BEGIN.
           
           PERFORM VARYING NUM-OF-STAR FROM 0 BY 1  UNTIL 
           NUM-OF-STAR >5
      *    10**4 = 10000
      *    10**3 = *1000
      *    10**2 = **100
      *    10**1 = ***10
      *    10**0 = ****1
              COMPUTE  STARS =  10 ** (4 -  NUM-OF-STAR )  
      *       INSPECT ** เปลื่ยน 1 หรือ 0 --> เป็น space
              INSPECT STARS  CONVERTING "10"   TO SPACES
              DISPLAY STARS 
      *       DISPLAY  NUM-OF-STAR "= " STARS
           END-PERFORM
           GOBACK 
       .
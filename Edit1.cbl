       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Edit1.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  NUM PIC   9(8)V99  VALUE 00014584.95.
       01  EDIT1 PIC   99,999,999.99 .   
       01  EDIT2 PIC   ZZ,ZZZ,ZZ9.99.
       01  EDIT3 PIC   $*,***,**9.99.
       01  EDIT4 PIC   ++,+++,++9.99.
       01  EDIT5 PIC   $$,$$$,$$9.99.
       01  EDIT6 PIC   $$,$$$,$$9.00.
       01  EDIT7 PIC   99/999/999.99.
       01  EDIT8 PIC   99999000999.99.
       01  EDIT9 PIC   99999BB999.99.


       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "MOVE NUM TO EDIT1"
           MOVE NUM to  EDIT1 
      *    00,014,584.95
           DISPLAY EDIT1
           DISPLAY "MOVE NUM TO EDIT2"
           MOVE NUM to  EDIT2 
      *    {}{},{}14,584,95
           DISPLAY EDIT2
           DISPLAY "MOVE NUM TO EDIT3"
           MOVE NUM to  EDIT3 
      *    $**14,584.95
           DISPLAY EDIT3
           DISPLAY "MOVE NUM TO EDIT4"
           MOVE NUM to  EDIT4 
      *    ${}{}{}+14584.95
           DISPLAY EDIT4
           DISPLAY "MOVE NUM TO EDIT5"
           MOVE NUM to  EDIT5 
      *    ${}{}$14,584.95
           DISPLAY EDIT5
           DISPLAY "MOVE NUM TO EDIT6"
           MOVE NUM to  EDIT6 
      *    ${}{}$14,584.00
           DISPLAY EDIT6
           DISPLAY "MOVE NUM TO EDIT7"
           MOVE NUM to  EDIT7 
      *    00/014/584.95
           DISPLAY EDIT7
           DISPLAY "MOVE NUM TO EDIT8"
           MOVE NUM to  EDIT8 
      *    00014000584.95
           DISPLAY EDIT8
           DISPLAY "MOVE NUM TO EDIT9"
           MOVE NUM to  EDIT9 
      *    00014{}{}584.95
           DISPLAY EDIT9
       
           
           GOBACK 
       .